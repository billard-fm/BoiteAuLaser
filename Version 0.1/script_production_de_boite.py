#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  decoupe_boite_a_bolpy_OBJET
#  
#  Copyright 2017 billard <billard@debian-0>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#
#
# FONCTIONNEL 
#



class boite:
	
	def __init__(self,largeur,profondeur,hauteur,pied, epaisseur,nom):
		self.hauteur=hauteur
		self.largeur=largeur
		self.pied=pied
		self.epaisseur=epaisseur
		self.nom=nom
		self.profondeur = profondeur
		self.demiHauteur = hauteur /2
		self.demiLargeur = largeur /2
		self.deuxE = 2 *epaisseur
		self.tiersLargeur = (largeur + self.deuxE)/3
		self.piedEpaisseur=pied+epaisseur
		self.dataBoite={}
		self.dataDimSVG={}
		
		#valeurs=[]
		#valeurs.append(largeur)
		#valeurs.append(hauteur+pied)
		#valeurs.append(self.profondeur)
		#valeurs.sort()
		#self.dimMax = valeurs[len(valeurs)-1] + 4 *epaisseur
		
	def dimSVG(self,nom):
		#print("TEST DES DONNES dim SVG")
		XYmax={}
		Xmax=0
		Ymax=0
		dataFace=self.dataBoite[nom]
		for contour in dataFace:
				#print("DONNEES DU CONTOUR EN TRAITEMENT")
				# print(contour)
				for data in contour:
					for XY in data:
						if (XY[0]>Xmax) : 
							Xmax=XY[0]
						if 	(XY[1]>Ymax) : 
							Ymax=XY[1]
		XYmax[0]=Xmax
		XYmax[1]=Ymax
		self.dataDimSVG[nom]=XYmax
		#print("X MAX :"+str(Xmax))				
		#print("Y MAX :"+str(Ymax))
		#print(self.dataDimSVG[nom])
		return(0)	
		
	def afficheData(self):
		print("Données de la boite ")
		print(self.hauteur) 
		print(self.largeur)
		print(self.epaisseur)
		print(self.pied)
		print(self.demiHauteur)
		print(self.demiLargeur)
		print(self.deuxE)
		print(self.tiersLargeur)
		print(self.piedEpaisseur)
		print(self.dataBoite)
	
	def donneesFond(self,nom):
		#OK
		dataFace=[]
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		Xmax=0
		Ymax=0
		
		X=0
		Y=0
		contourData.append([[X,Y]])
		
		X=self.largeur
		contourData.append([[X,Y]])
		
		Y=self.profondeur
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
		
		dataFace.append(contourData)
		
		
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		
		
		
		
		print("DONNEES FINALES")
		print(dataFace)
		self.dataBoite[nom]=dataFace
		
		self.dimSVG(nom)
	
	def donneesCoteA(self,nom):
		#OK
		dataFace=[]
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		X=0
		Y=0
		contourData.append([[X,Y]])
		X=self.largeur+self.deuxE
		
		contourData.append([[X,Y]])
		
		Y=self.hauteur+self.pied+self.epaisseur
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
	
		
		dataFace.append(contourData)
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		
		
		
		
		print("DONNEES FINALES")
		print(dataFace)
		self.dataBoite[nom]=dataFace
		self.dimSVG(nom)
		
		
	def donneesCouvercle(self,nom):
		#OK
		dataFace=[]
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		X=0
		Y=0
		contourData.append([[X,Y]])
		
		X=self.largeur+self.deuxE
		contourData.append([[X,Y]])
		
		Y=self.profondeur+self.deuxE
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
		
		
		dataFace.append(contourData)
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		
		
		
		
		print("DONNEES FINALES")
		print(dataFace)
		self.dataBoite[nom]=dataFace
		self.dimSVG(nom)	
	
	def donneesCoteB(self,nom):
		#OK
		dataFace=[]
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		X=0
		Y=0
		contourData.append([[X,Y]])
		X=self.profondeur+self.deuxE
		
		contourData.append([[X,Y]])
		
		Y=self.hauteur+self.pied+self.epaisseur
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
	
		
		dataFace.append(contourData)
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		
		
		
		
		print("DONNEES FINALES")
		print(dataFace)
		self.dataBoite[nom]=dataFace
		self.dimSVG(nom)
		
		
	def svgCreation(self):
		#modification ici le Gcode sera produit a partir d'un tableau complet des six faces.
		print("production du SVG");	
		
		
		
		# parcours des données du couvercle
		# Il peut y avoir plusieurs contours.
		#ICI il faut parcourir la varaible self.dataBoite pour obtenir toutes les faces.
		for key, value in self.dataBoite.items():
			print "Clé : %s, valeur : %s." % (key, value)	
			dataFace=value
			nomFace=key
			#print("donnés des dim max du SVG")	
			XYmax=self.dataDimSVG[key]
			#print(XYmax)
			numeroContour=0
			nom=self.nom+"_"+str(self.largeur)+"_"+str(self.profondeur)+"_"+str(self.hauteur)+"_"+str(self.epaisseur)+"_"+str(self.pied)+"_"+nomFace
			nomFichier=nom+".svg"
			f=open(nomFichier,'w');
			print("PRODUCTION DU SVC")
			#introduction des metadonnée
			f.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<svg")
			f.write("	xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n")
			f.write("	xmlns:cc=\"http://creativecommons.org/ns#\"\n")
			f.write("	xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n")
			f.write("	xmlns:svg=\"http://www.w3.org/2000/svg\"\n")
			f.write("	xmlns=\"http://www.w3.org/2000/svg\"\n")
			f.write("	xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n")
			f.write("	xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n")
			f.write("	width=\""+str(XYmax[0])+"\"\n")
   			f.write("	height=\""+str(XYmax[1])+"\"\n")
   			f.write("	id=\"box\"\n")
   			f.write("	version=\"1.1\"\n")
   			f.write("	inkscape:version=\"0.92.1 r15371\"\n")
   			f.write("	sodipodi:docname=\""+nom+"\">\n")
  			
  			f.write("<defs \n")
			f.write("	id=\"defs4\" />\n")
			
			f.write("	<sodipodi:namedview\n")
			f.write("    id=\"base\"\n")
     		
			f.write("	pagecolor=\"#ffffff\"\n")
			
			f.write("	bordercolor=\"#666666\"\n")
			f.write("	borderopacity=\"1.0\"\n")
			f.write("	inkscape:pageopacity=\"0.0\"\n")
			f.write("	inkscape:pageshadow=\"2\"\n")
			f.write("	inkscape:document-units=\"mm\"\n")
			f.write("	inkscape:current-layer=\"layer1\"\n")
			f.write("	showgrid=\"false\"\n")
			f.write("	fit-margin-top=\"0\"\n")
			f.write("	fit-margin-left=\"0\"\n")
			f.write("	fit-margin-right=\"0\"\n")
			f.write("	fit-margin-bottom=\"0\"\n")
			f.write("	units=\"mm\"\n")
			f.write("	inkscape:window-maximized=\"1\"\n")
			f.write("	inkscape:zoom=\"0.85391197\"\n")
			f.write("	inkscape:cx=\"772.91339\"\n")
			f.write("	inkscape:cy=\"400.62991\"\n")
			f.write("	inkscape:window-width=\"1920\"\n")
			f.write("	inkscape:window-height=\"1023\"\n")
			f.write("	inkscape:window-x=\"0\"\n")
			f.write("	inkscape:window-y=\"24\" />\n\n")
			f.write("<metadata\n")
			f.write("	id=\"metadata7\">\n")
			f.write("	<rdf:rdf>\n")
			f.write("	<cc:work\n")
			f.write("	rdf:about=\"\">\n")
			f.write("	<dc:format>image/svg+xml</dc:format>\n")
			f.write("	<dc:type\n")
			f.write("	rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n")	
			f.write("	<dc:title />\n")
			f.write("	</cc:work>\n")
			f.write("	</rdf:rdf>\n")
			f.write("	<rdf:RDF>\n")
			f.write("	<cc:Work\n")
			f.write("	rdf:about=\"\">\n")
			f.write("	<dc:format>image/svg+xml</dc:format>\n")
			f.write("	<dc:type\n")
			f.write("	rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n")
			f.write("	<dc:title></dc:title>\n")
			f.write("	</cc:Work>\n")
			f.write("	</rdf:RDF>\n")
			f.write("	</metadata>\n\n\n")
			f.write("<g\n")
  
			f.write("	inkscape:label=\"Calque 1\"\n")
			f.write("	inkscape:groupmode=\"layer\"\n") 
			f.write("	id=\"layer1\">\n")
			
			for contour in dataFace:
				#print("DONNEES DU CONTOUR EN TRAITEMENT")
				print(contour)
				numeroContour=numeroContour+1	
				#f.write("DECOUPE "+str(numeroContour)+" \n\n")
				f.write("	<path\n")
				f.write("		style=\"fill:none;stroke:#ff0000;stroke-width:1.06666672\"\n")
				f.write("		id=\""+str(nomFace)+"\"\n")
			
				i=0
				for data in contour:
					print(data)
					for XY in data:
						print(XY)
						X=str(XY[0])
						Y=str(XY[1])
						print("i :"+str(i))
						#aller sans le laser au premier point du tableau
						if (i==0):
							f.write("	d=\"M "+X+","+Y+" ")	
						else :
							f.write(X+','+Y+' ')
					i=i+1
				#f.write("FIN DE LA DECOUPE "+str(numeroContour)+"\n\n")
				f.write("\"\n       inkscape:connector-curvature=\"0\" />");
				f.write("\n	</path>\n")
			
			
			
			f.write("\n</g> \n </svg>");
  
			f.close()
		return 0	
		
	def gCodeCreation(self):
		#modification ici le Gcode sera produit a partir d'un tableau complet des six faces.
		print("production du gCode");	
		#production du Gcode du couvercle
		
		# parcours des données du couvercle
		# Il peut y avoir plusieurs contours.
		#ICI il faut parcourir la varaible self.dataBoite pour obtenir toutes les faces.
		for key, value in self.dataBoite.items():
			print "Clé : %s, valeur : %s." % (key, value)	
			dataFace=value
			nomFace=key
			numeroContour=0
			nomFichier=self.nom+"_"+str(self.largeur)+"_"+str(self.hauteur)+"_"+str(self.epaisseur)+"_"+str(self.pied)+"_"+nomFace+".gcode"
			f=open(nomFichier,'w');
			print("PRODUCTION DU GCODE")
		
			f.write("Gcode du couvercle pour une boite de dimensions\n")
			f.write("(largeur : "+str(self.largeur)+")\n")
			f.write("(hauteur : "+str(self.hauteur)+")\n")
			f.write("(epaisseur bois : "+str(self.epaisseur)+")\n")
			f.write("(hauteur pied : "+str(self.pied)+")\n")
		
			#positionnement initial
			f.write("Prise d'origine\n")
			f.write("G90\nG92 X0 Y0 Z0\nG21\nG0 Z0.0 F4000\n\n")
			for contour in dataFace:
				#print("DONNEES DU CONTOUR EN TRAITEMENT")
				print(contour)
				numeroContour=numeroContour+1	
				#f.write("DECOUPE "+str(numeroContour)+" \n\n")
				f.write("M10")
				i=0
				for data in contour:
					print(data)
					for XY in data:
						print(XY)
						X=str(XY[0])
						Y=str(XY[1])
						print("i :"+str(i))
						#aller sans le laser au premier point du tableau
						if (i==0):
							f.write("G1  F500.000000 S0.0\n")	
							#decoupe jusqu'au point suivant
							f.write("G1  X"+X+"  Y"+Y+" F500.000000 S0.0\n")
							f.write("M11\n\n")
						else :
							f.write("\nM10\n")
							#Mise en vitesse de 500 et puissance de 0,7
							f.write("G1  F500.000000 S0.7\n")	
							#decoupe jusqu'au point suivant
							f.write("G1  X"+X+"  Y"+Y+" F500.000000 S0.7\n")
							f.write("M11\n\n")
							f.write("G0  X"+X+"  Y"+Y)
							f.write("\n")
					i=i+1
				#f.write("FIN DE LA DECOUPE "+str(numeroContour)+"\n\n")
			
			f.write("G0 X0.000 Y0.000 Z0.000\nM05\nM02\n")
			f.close()	


class boite_type1(boite):
	def donneesFond(self,nom):
		#OK
		dataFace=[]
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		Xmax=0
		Ymax=0
		
		X=self.epaisseur
		Y=self.epaisseur
		contourData.append([[X,Y]])
		
		X=X+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
		X=X+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=self.epaisseur
		contourData.append([[X,Y]])
		
		X=self.epaisseur+self.largeur
		contourData.append([[X,Y]])
		
		Y=self.epaisseur+self.profondeur/3
		contourData.append([[X,Y]])
		
		X=X+self.epaisseur
		contourData.append([[X,Y]])
		
		Y=Y+self.profondeur/3
		contourData.append([[X,Y]])
		
		X=X-self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.epaisseur+self.profondeur
		contourData.append([[X,Y]])
		
		X=X-self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y+self.epaisseur
		contourData.append([[X,Y]])
		
		X=self.epaisseur+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=self.profondeur+self.epaisseur
		contourData.append([[X,Y]])
		
		X=self.epaisseur
		contourData.append([[X,Y]])
		
		Y=2*self.profondeur/3+self.epaisseur
		contourData.append([[X,Y]])
		
		
		X=0
		contourData.append([[X,Y]])
		
		Y=self.epaisseur+self.profondeur/3
		contourData.append([[X,Y]])
	
		X=self.epaisseur
		contourData.append([[X,Y]])
	
		Y=self.epaisseur
		contourData.append([[X,Y]])
	
		
		dataFace.append(contourData)
		
		
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		
		
		
		
		print("DONNEES FINALES")
		print(dataFace)
		self.dataBoite[nom]=dataFace
		
		self.dimSVG(nom)
	
	def donneesCoteA(self,nom):
		#OK
		print("#################################################")
		print(nom)
		print("#################################################")
		
		#trou central
		contourData=[]
		dataFace=[]
		print("EFFACEMENT")
		print(contourData)
		print(dataFace)
		X=self.largeur/3 + self.epaisseur
		Y=self.pied
		contourData.append([[X,Y]])
		
		X=X+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y+self.epaisseur
		contourData.append([[X,Y]])
		
		X=self.largeur/3 + self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.pied
		contourData.append([[X,Y]])
		
		print("TROU")
		print(contourData)
		print(dataFace)
		print("#################################################")
		dataFace.append(contourData)
		
		
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		
		X=self.epaisseur
		Y=0
		contourData.append([[X,Y]])
		
		X=X+self.largeur+self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.hauteur/2+self.pied+self.epaisseur
		contourData.append([[X,Y]])
		
		X=X-self.epaisseur
		contourData.append([[X,Y]])
		
		Y=Y+self.hauteur/2
		contourData.append([[X,Y]])
		
		X=X-self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y+self.epaisseur
		contourData.append([[X,Y]])
		
		X=X-self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y-self.epaisseur
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=Y-self.hauteur/2
		contourData.append([[X,Y]])
		
		X=self.epaisseur
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
		
		dataFace.append(contourData)
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		self.dataBoite[nom]=dataFace
		
		self.dimSVG(nom)
	
		
		
		
	def donneesCoteB(self,nom):
		#OK
		print("#################################################")
		print(nom)
		print("#################################################")
		#trou central
		contourData=[]
		dataFace=[]
		print("EFFACEMENT")
		print(contourData)
		print(dataFace)
		X=self.largeur/3 + self.epaisseur
		Y=self.pied
		contourData.append([[X,Y]])
		
		X=X+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y+self.epaisseur
		contourData.append([[X,Y]])
		
		X=self.largeur/3 + self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.pied
		contourData.append([[X,Y]])
		
		print("TROU")
		print(contourData)
		print(dataFace)
		
		dataFace.append(contourData)
		
		
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		
		X=self.epaisseur
		Y=0
		contourData.append([[X,Y]])
		
		X=X+self.largeur+self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.hauteur/2+self.pied+self.epaisseur
		contourData.append([[X,Y]])
		
		X=X-self.epaisseur
		contourData.append([[X,Y]])
		
		Y=Y+self.hauteur/2
		contourData.append([[X,Y]])
		
		X=X-self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y+self.epaisseur
		contourData.append([[X,Y]])
		
		X=X-self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y-self.epaisseur
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=Y-self.hauteur/2
		contourData.append([[X,Y]])
		
		X=self.epaisseur
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
		
		dataFace.append(contourData)
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)	
		
		
		#print("DONNEES FINALES")
		#print(dataFace)
		self.dataBoite[nom]=dataFace
		self.dimSVG(nom)
		print("#################################################")
		
	def donneesCouvercle(self,nom):
		#OK
		dataFace=[]
		contourData=[]
		print("DONNEE INITIALES")
		print(dataFace)
		print(contourData)
		X=0
		Y=0
		contourData.append([[X,Y]])
		
		X=X+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=self.epaisseur
		contourData.append([[X,Y]])
		
		X=X+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=0
		contourData.append([[X,Y]])
		
		X=self.epaisseur+self.largeur
		contourData.append([[X,Y]])
		
		Y=self.epaisseur+self.profondeur/3
		contourData.append([[X,Y]])
		
		X=X-self.epaisseur
		contourData.append([[X,Y]])
		
		Y=Y+self.profondeur/3
		contourData.append([[X,Y]])
		
		X=X+self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.epaisseur+self.profondeur
		contourData.append([[X,Y]])
		
		X=X-self.largeur/3
		contourData.append([[X,Y]])
		
		Y=Y-self.epaisseur
		contourData.append([[X,Y]])
		
		X=self.epaisseur+self.largeur/3
		contourData.append([[X,Y]])
		
		Y=self.profondeur+self.epaisseur
		contourData.append([[X,Y]])
		
		X=0
		contourData.append([[X,Y]])
		
		Y=2*self.profondeur/3+self.epaisseur
		contourData.append([[X,Y]])
		
		
		X=self.epaisseur
		contourData.append([[X,Y]])
		
		Y=self.epaisseur+self.profondeur/3
		contourData.append([[X,Y]])
	
		X=0
		contourData.append([[X,Y]])
	
		Y=0
		contourData.append([[X,Y]])
		
		
		
		dataFace.append(contourData)
		
		print("Donnes de contour")
		print(contourData)
		print(dataFace)
		
		
		
		
		print("DONNEES FINALES")
		print(dataFace)
		self.dataBoite[nom]=dataFace
		self.dimSVG(nom)	
	
	
		
		

		
def main(args):
	print("Production gcode pour boite à bol")
	#largeur=input("Largeur(en mm) : ")
	#hauteur=input("Hauteur (en mm) : ")
	#profondeur=input("Profondeur (en mm) : ")
	#pied=input("Pied (en mm) :")
	#epaisseur=input("Epaisseur boite (en mm) :")
	#nom = input("Nom de la boite")
	
	#pour les tests 
	largeur = 100
	profondeur = 110
	hauteur =70
	pied=10
	epaisseur=5
	
	
	boite1 = boite_type1(largeur,profondeur,hauteur,pied,epaisseur,"boite_generique")
	boite1.donneesFond("FOND")
	boite1.donneesCoteA("COTE_A")
	boite1.donneesCoteB("COTE_B")
	boite1.donneesCouvercle("COUVERCLE")
	
	
	
	boite1.afficheData()
	#boite1.gCodeCreation()
	boite1.svgCreation()
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
